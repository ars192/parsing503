module parsing503

go 1.18

require (
    github.com/gocolly/colly v1.2.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.3.5
	github.com/spf13/viper v1.14.0
)