package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/go-delve/delve/pkg/config"
)

const (
	tgBotHost = "api.telegram.org"
	batchSize = 100
)

func main() {

	// load configs
	conf, err := config.LoadConfig()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(conf)
	}

	// init postgres db
	// create pool of connection for DB
	dbpool, err := postgres.InitPostgresDBConn(&conf)
	if err != nil {
		log.Fatalf("database: %v", err)
	}
	defer dbpool.Close()

	s := postgres.NewDBArticleRepo(dbpool)

	// init parser
	var parsePeriod time.Duration = 60 // parsing period in minute
	go parser.NewParser(s, parsePeriod)

	// init API fetcher
	go apifetcher.NewFetcher(s, parsePeriod)

	eventsProcessor := telegram.New(
		tgClient.New(tgBotHost, mustToken()),
		s,
	)

	log.Print("service started")

	consumer := event_consumer.New(eventsProcessor, eventsProcessor, batchSize)

	if err := consumer.Start(); err != nil {
		log.Fatal("service is stopped", err)
	}

}

func mustToken() string {
	token := flag.String(
		"tg-bot-token",
		"",
		"token for access to telegram bot",
	)

	flag.Parse()

	if *token == "" {
		log.Fatal("token is not specified")
	}

	return *token
}
